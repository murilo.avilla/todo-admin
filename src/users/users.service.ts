import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.schema';
import { Model } from 'mongoose';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async findUserById(userId: string): Promise<User> {
    try {
      return await this.userModel.findById(userId);
    } catch (error) {
      throw new RpcException(error.message);
    }
  }
}
