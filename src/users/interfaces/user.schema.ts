import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

@Schema({ collection: 'User' })
export class User {
  @Prop()
  name: string;

  @Prop()
  image?: string;

  @Prop()
  email: string;

  @Prop()
  emailVerified: Date;

  @Prop()
  hashedPassword: string;

  @Prop()
  createdAt: Date;

  @Prop()
  updatedAt: Date;
}

export type UserDocument = HydratedDocument<User>;

export const UserSchema = SchemaFactory.createForClass(User);
