import { Controller } from '@nestjs/common';
import { CollectionsService } from './collections.service';
import {
  Ctx,
  EventPattern,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { Collection } from './interfaces/collection.schema';

const ackErrors: string[] = ['E11000'];

@Controller()
export class CollectionsController {
  constructor(private readonly collectionsService: CollectionsService) {}

  @EventPattern('collection-create')
  async createCollection(
    @Payload() collection: Collection,
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.collectionsService.createCollection(collection);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }

  @MessagePattern('collection-find')
  async findCollection(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      return await this.collectionsService.findCollection(
        body?.userId,
        body?.collectionId,
      );
    } finally {
      await channel.ack(originalMessage);
    }
  }

  @EventPattern('collection-update')
  async updateCollection(
    @Payload('user') userId: string,
    @Payload('collectionId') _id: string,
    @Payload('collection') collection: any,
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.collectionsService.updateCollection(userId, _id, collection);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }
}
