import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from '../../users/interfaces/user.schema';

@Schema({ timestamps: true, collection: 'Collections' })
export class Collection {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop()
  name: string;

  @Prop({ default: true })
  status: boolean;
}

export type CollectionDocument = HydratedDocument<Collection>;

export const CollectionSchema = SchemaFactory.createForClass(Collection);
