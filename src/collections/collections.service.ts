import { Injectable, Logger } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Collection } from 'src/collections/interfaces/collection.schema';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class CollectionsService {
  private logger = new Logger(CollectionsService.name);

  constructor(
    @InjectModel('Collection')
    private readonly collectionModel: Model<Collection>,
    private readonly userService: UsersService,
  ) {}

  async createCollection(collection: Collection) {
    try {
      const createdCollection = new this.collectionModel(collection);
      createdCollection.status = true;
      await createdCollection.save();
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }

  async findCollection(userId: string, _id?: string) {
    try {
      const user = await this.userService.findUserById(userId);
      const filter = { user };
      if (_id) filter['_id'] = _id;
      return await this.collectionModel.find(filter);
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }

  async findCollectionById(collectionId: string) {
    try {
      return await this.collectionModel.findById(collectionId);
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }

  async updateCollection(userId: string, _id: string, collection: any) {
    try {
      const user = await this.userService.findUserById(userId);
      return await this.collectionModel.findOneAndUpdate(
        { user, _id },
        { $set: collection },
      );
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }
}
