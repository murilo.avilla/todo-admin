import { Module } from '@nestjs/common';
import { CollectionsController } from './collections.controller';
import { CollectionsService } from './collections.service';
import { UsersModule } from 'src/users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CollectionSchema } from './interfaces/collection.schema';

@Module({
  controllers: [CollectionsController],
  providers: [CollectionsService],
  imports: [
    UsersModule,
    MongooseModule.forFeature([
      {
        name: 'Collection',
        schema: CollectionSchema,
      },
    ]),
  ],
  exports: [CollectionsService],
})
export class CollectionsModule {}
