import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { UsersModule } from 'src/users/users.module';
import { CollectionsModule } from 'src/collections/collections.module';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskSchema } from './interfaces/task.schema';

@Module({
  controllers: [TaskController],
  providers: [TaskService],
  imports: [
    UsersModule,
    CollectionsModule,
    MongooseModule.forFeature([
      {
        name: 'Task',
        schema: TaskSchema,
      },
    ]),
  ],
})
export class TaskModule {}
