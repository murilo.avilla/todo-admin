import { Controller, Logger } from '@nestjs/common';
import { TaskService } from './task.service';
import {
  Ctx,
  EventPattern,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { Task } from './interfaces/task.schema';

const ackErrors: string[] = ['E11000'];

@Controller()
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  private logger = new Logger(TaskController.name);

  @EventPattern('task-create')
  async createTask(@Payload() task: Task, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.taskService.createTask(task);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }

  @MessagePattern('task-find')
  async findTask(@Payload() payload: string[], @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      return await this.taskService.findTask(
        payload['userId'],
        payload['collectionId'],
        payload['_id'],
      );
    } finally {
      await channel.ack(originalMessage);
    }
  }

  @EventPattern('task-update')
  async updateTask(@Payload() payload: object, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.taskService.updateTask(payload['taskId'], payload['task']);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }
}
