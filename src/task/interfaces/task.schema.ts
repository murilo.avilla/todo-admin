import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from '../../users/interfaces/user.schema';
import { Collection } from '../../collections/interfaces/collection.schema';

@Schema({
  timestamps: true,
  collection: 'Tasks',
})
export class Task {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Collection' })
  collectionId: Collection;

  @Prop()
  name: string;

  @Prop()
  order: number;

  @Prop()
  status: string;
}

export type TaskDocument = HydratedDocument<Task>;

export const TaskSchema = SchemaFactory.createForClass(Task);
