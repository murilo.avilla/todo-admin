import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './interfaces/task.schema';
import { RpcException } from '@nestjs/microservices';
import { CollectionsService } from 'src/collections/collections.service';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class TaskService {
  private logger = new Logger(TaskService.name);

  constructor(
    @InjectModel('Task') private readonly taskModel: Model<Task>,
    private readonly collectionService: CollectionsService,
    private readonly userService: UsersService,
  ) {}

  async createTask(task: Task) {
    try {
      const createdTask = new this.taskModel(task);
      createdTask.status = createdTask.status ? createdTask.status : 'PENDING';

      const countTasks = await this.taskModel.count({
        collectionId: createdTask.collectionId,
        user: createdTask.user,
      });

      createdTask.order = createdTask.order
        ? createdTask.order
        : countTasks + 1;

      await createdTask.save();
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }

  async findTask(userId: string, collectionId?: string, _id?: string) {
    try {
      const filter = { user: userId };

      if (collectionId) filter['collectionId'] = collectionId;

      if (_id) filter['_id'] = _id;

      return await this.taskModel.find(filter);
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }

  async updateTask(_id: string, task: any) {
    try {
      return await this.taskModel.findOneAndUpdate({ _id }, { $set: task });
    } catch (error) {
      this.logger.error(error.message);
      throw new RpcException('Error');
    }
  }
}
